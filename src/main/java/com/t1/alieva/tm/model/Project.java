package com.t1.alieva.tm.model;

import com.t1.alieva.tm.api.model.IWBS;
import com.t1.alieva.tm.enumerated.Status;
import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractUserOwnedModel implements IWBS {

    @NotNull
    private Date created = new Date();
    @NotNull
    private String description = "";

    @NotNull
    private String name = "";

    @NotNull
    private Status status = Status.NOT_STARTED;

    public Project() {
    }

    public Project(@NotNull String name) {
        this.name = name;
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    public Project(@NotNull String name, @NotNull Status status) {
        this.name = name;
        this.status = status;
    }

    @Override
    public String toString() {
        return name + " : " + description;
    }

}
