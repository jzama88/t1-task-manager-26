package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "p-show-by-id";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show Project by ID.";
    }

    @Override
    public void execute() throws AbstractFieldException, AbstractUserException {
        System.out.println(("[SHOW PROJECT BY ID]"));
        System.out.println(("[ENTER ID]"));
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final String userId = getUserId();
        @Nullable final Project project = getProjectService().findOneById(userId, id);
        showProject(project);
    }
}
