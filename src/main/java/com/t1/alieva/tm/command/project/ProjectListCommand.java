package com.t1.alieva.tm.command.project;

import com.t1.alieva.tm.enumerated.ProjectSort;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String getName() {
        return "project-list";
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Show project list.";
    }

    @Override
    public void execute() throws AbstractUserException, AbstractFieldException {
        System.out.println("[PROJECT LIST]");
        System.out.println("[ENTER SORT]");
        System.out.println(Arrays.toString(ProjectSort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final ProjectSort sort = ProjectSort.toSort(sortType);
        @NotNull final String userId = getUserId();
        final List<Project> projects = getProjectService().findAll(userId, sort.getComparator());
        int index = 1;
        for (@NotNull final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
    }
}
