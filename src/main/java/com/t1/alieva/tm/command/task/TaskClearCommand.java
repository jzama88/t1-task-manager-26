package com.t1.alieva.tm.command.task;

import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getName() {
        return "task-clear";
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException {
        System.out.println("[CLEAR TASK]");
        @NotNull final String userId = getUserId();
        getTaskService().removeAll(userId);
    }
}
