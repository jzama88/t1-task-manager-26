package com.t1.alieva.tm.command.user;

import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Change user password.";

    @NotNull
    public static final String NAME = "user-change-password";

    @Override
    public void execute() throws
            AbstractUserException,
            AbstractFieldException,
            AbstractEntityNotFoundException {
        System.out.println("[CHANGE USER PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String newPassword = TerminalUtil.nextLine();
        @Nullable final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getUserService().setPassword(userId, newPassword);
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }
}
