package com.t1.alieva.tm.enumerated;

import com.t1.alieva.tm.comparator.CreatedComparator;
import com.t1.alieva.tm.comparator.NameComparator;
import com.t1.alieva.tm.comparator.StatusComparator;
import com.t1.alieva.tm.model.Project;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;

public enum ProjectSort {

    BY_CREATED("Sort by created", CreatedComparator.INSTANCE::compare),
    BY_NAME("Sort by name", NameComparator.INSTANCE::compare),
    BY_STATUS("Sort by status", StatusComparator.INSTANCE::compare);

    @Getter
    @NotNull
    private final String displayName;

    @Getter
    @NotNull
    private final Comparator<Project> comparator;

    ProjectSort(
            @NotNull final String displayName,
            @NotNull final Comparator<Project> comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @Nullable
    public static ProjectSort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final ProjectSort sort : values()) {
            if (sort.name().equals(value)) return sort;
        }
        return null;
    }
}
