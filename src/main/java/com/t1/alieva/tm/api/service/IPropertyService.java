package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.api.component.ISaltProvider;
import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
