package com.t1.alieva.tm.component;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.api.repository.IProjectRepository;
import com.t1.alieva.tm.api.repository.ITaskRepository;
import com.t1.alieva.tm.api.repository.IUserRepository;
import com.t1.alieva.tm.api.service.*;
import com.t1.alieva.tm.command.AbstractCommand;
import com.t1.alieva.tm.enumerated.Role;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.exception.system.ArgumentNotSupportedException;
import com.t1.alieva.tm.exception.system.CommandNotSupportedException;
import com.t1.alieva.tm.exception.user.AbstractUserException;
import com.t1.alieva.tm.exception.user.AuthenticException;
import com.t1.alieva.tm.model.User;
import com.t1.alieva.tm.repository.CommandRepository;
import com.t1.alieva.tm.repository.ProjectRepository;
import com.t1.alieva.tm.repository.TaskRepository;
import com.t1.alieva.tm.repository.UserRepository;
import com.t1.alieva.tm.service.*;
import com.t1.alieva.tm.util.SystemUtil;
import com.t1.alieva.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;

import javax.naming.AuthenticationException;
import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;


public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "com.t1.alieva.tm.command";

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Getter
    private final ICommandService commandService = new CommandService(
            commandRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    @Getter
    private final IProjectService projectService = new ProjectService(
            projectRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    @Getter
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    @Getter
    private final IProjectTaskService projectTaskService = new ProjectTaskService(
            projectRepository,
            taskRepository
    );

    @NotNull
    @Getter
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    @Getter
    private final IUserService userService = new UserService(
            propertyService,
            userRepository,
            taskRepository,
            projectRepository
    );

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(
            propertyService,
            userService
    );

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) {
            registry(clazz);
        }
    }

    @SneakyThrows
    private void initPID(){
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename),pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    private void processCommand(@NotNull final String command) throws
            AbstractFieldException,
            AbstractEntityNotFoundException,
            CommandNotSupportedException,
            AbstractUserException,
            AuthenticException, AuthenticationException {
        @Nullable final AbstractCommand abstractcommand = commandService.getCommandByName(command);
        if (abstractcommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractcommand.getRoles());
        abstractcommand.execute();
    }

    private void initLogger() {
        loggerService.info("**WELCOME TO TASK-MANAGER**");
        Runtime.getRuntime().addShutdownHook(new Thread(() -> loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **")));
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @Nullable final AbstractCommand command = clazz.newInstance();
        registry(command);
    }


    private void processArgument(@NotNull final String argument) throws
            ArgumentNotSupportedException,
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException,
            AuthenticException, AuthenticationException {
        @Nullable final AbstractCommand abstractcommand = commandService.getCommandByArgument(argument);
        if (abstractcommand == null) throw new ArgumentNotSupportedException(argument);
        abstractcommand.execute();
    }

    public void run(@Nullable final String[] args) throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            ArgumentNotSupportedException,
            AbstractUserException, AuthenticationException, AuthenticException {
        if (processArguments(args)) System.exit(0);

        initPID();
        initDemoData();
        initLogger();

        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("OK");
            } catch (@NotNull final Exception e) {
                loggerService.error(e);
                System.out.println("FAIL");
            }
        }
    }

    private void initDemoData() throws
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException {

        @NotNull final User userTest = userService.create("test", "test");
        @NotNull final User userCustom = userService.create("user", "user", "user@user.ru");
        @NotNull final User userAdmin = userService.create("admin", "admin", Role.ADMIN);

        projectService.create(userTest.getId(), "PROJECT_TEST1", "Project 1 for TestUser");
        projectService.create(userTest.getId(), "PROJECT_TEST2", "Project 2 for TestUser");
        projectService.create(userCustom.getId(), "PROJECT_CUSTOM1", "Project for CustomUser");
        projectService.create(userAdmin.getId(), "PROJECT_ADMIN", "Project 1 for Admin");
        projectService.create(userAdmin.getId(), "PROJECT_ADMIN", "Project 2 for Admin");

        taskService.create(userTest.getId(), "TASK_TEST1", "test task 1");
        taskService.create(userTest.getId(), "TASK_TEST2", "test task 2");
        taskService.create(userCustom.getId(), "TASK_CUSTOM1", "test task 1");
        taskService.create(userCustom.getId(), "TASK_CUSTOM2", "test task 2");
        taskService.create(userAdmin.getId(), "TASK_ADMIN1", "test task 1");
        taskService.create(userAdmin.getId(), "TASK_ADMIN2", "test task 2");

    }

    private boolean processArguments(@Nullable final String[] args) throws
            ArgumentNotSupportedException,
            AbstractEntityNotFoundException,
            AbstractFieldException,
            AbstractUserException, AuthenticationException, AuthenticException {
        if (args == null || args.length == 0) return false;
        processArgument(args[0]);
        return true;
    }

}
